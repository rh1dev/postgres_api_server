var express = require('express')
var pg = require('pg')
// queries
var dashboard = require('./queries/dashboard.js')
var yearchart = require('./queries/yearchart.js')
var daychart = require('./queries/daychart.js')
var hourchart = require('./queries/hourchart.js')
var rawdata = require('./queries/rawData.js')

var app = express()

var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
}

app.use(allowCrossDomain);

var connectionString = "postgres://rh1:Anal1st-R0undH0use@rds-rh1.4dapt.com/rh1"

var client = new pg.Client(connectionString);
client.connect()

// db client
app.use(function(req, res, next) {
  req.client = client
  // db table prefix
  // uh manoa
  req.tableName = 'uhm';
  // stanford
  // req.tableName = 'gse';
  next()
})

// sql where clause
app.use(function(req, res, next) {
  req.whereClause = '';
  if (req.query.room_id) {
    req.whereClause = 'WHERE r.room_id=\'' + req.query.room_id + '\'';
  }
  else if (req.query.bldg_id) {
    req.whereClause = 'WHERE r.bldg_id=\'' + req.query.bldg_id + '\'';
  }
  next()
})

// get all class data for dashboard
app.get('/class', function(req, res, next) {
  dashboard(req, res, next)
})
// year chart
app.get('/year', function(req, res, next) {
  yearchart(req, res, next)
})
// day chart
app.get('/day', function(req, res, next) {
  daychart(req, res, next)
})
// hour chart
app.get('/hour', function(req, res, next) {
  hourchart(req, res, next)
})
// raw data
app.get('/data', function(req, res, next) {
  rawdata(req, res, next)
})

app.get('/', function (req, res) {
  res.send('postgres api server')
})

app.listen(5003, function () {
  console.log('Example app listening on port 5003!')
})
