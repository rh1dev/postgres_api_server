module.exports = function(req, res, next) {

  var select = [
    // 'SELECT AVG(c.class_enroll) / AVG(r.capacity) AS utilization,',
    'SELECT AVG(c.class_enroll) / AVG(r.mCap) AS utilization,',
    'EXTRACT(DOW FROM c.start_time) AS time_day',
    'FROM ' + req.tableName + '_room AS r',
    'INNER JOIN ' + req.tableName + '_class AS c',
    'ON r.room_id = c.room_id', 
    req.whereClause,
    'GROUP BY time_day',
    'ORDER BY time_day'
  ].join(' ');

  var query = req.client.query(select);

  query.then(function(data) {
    res.send(data.rows)
  })
}
