var _ = require('underscore')
var convertToNumber = require('../helpers/convertToNumber.js')
var convertToMinutes = require('../helpers/convertToMinutes.js')
var roomStats = require('../helpers/roomStats.js')
var siteStats = require('../helpers/siteStats.js')
var buildingStats = require('../helpers/buildingStats.js')

module.exports = function(req, res, next) {

    var timeAvailable = {
        minutes: (672 * 60),
        hours: 672,
        weeks: 10.87,
        days: (10.87 * 5)
    }

  // using max as aggregate funciton instead of last (not working in postgres)
  var select = [
    'SELECT',
    'COUNT(c.class_id) total_classes,',
    'MAX(b.bldg_id) as building,',
    'MAX(b.bldg_alias) as building_alias,',
    'MAX(b.latitude) as latitude,',
    'MAX(b.longitude) as longitude,',
    'MAX(r.scheduled_y) as scheduled,',
    'MAX(r.room_id) as room,',
    'MAX(r.room_type) as room_type,',
    'MAX(r.area_sf) as room_gsf,',
    // 'MAX(r.capacity) as capacity,', /// was mCap
    'MAX(r.mCap) as capacity,', /// was mCap
    'AVG(class_enroll) as enrollment,',
    // '(AVG(class_enroll) / AVG(r.capacity)) as density,',
    '(AVG(class_enroll) / AVG(r.mCap)) as density,',
    // '(MAX(r.area_sf) / MAX(r.capacity)) as density_gsf,',
    '(MAX(r.area_sf) / MAX(r.mCap)) as density_gsf,',
    '(MAX(r.area_sf) / AVG(class_enroll)) as gsfPerPersonUsed,',
    'SUM(end_time - start_time) as duration', 
    'FROM ' + req.tableName + '_class AS c',
    'RIGHT OUTER JOIN ' + req.tableName + '_room AS r',
    'ON c.room_id = r.room_id',
    'JOIN ' + req.tableName + '_building AS b',
    'ON b.bldg_id = r.bldg_id',
    // req.whereClause,
    'WHERE r.room_type = \'Classroom\'',
    'OR r.room_type = \'Class Laboratory\'',
    'GROUP BY r.room_id'
  ].join(' ');

  var query = req.client.query(select);

  query.then(function(data) {

    // need to convert aggregate values back to number
    convertToNumber(data)
    // convert duration object to minutes
    convertToMinutes(data)
    // additional room stats
    roomStats(data, timeAvailable)

    var feed = {}
    feed.rows = data.rows

    // hard-coding operation hours
    // uhmanoa
    // hard-coding (m-f) 8am-8pm 8/26/14 - 11/10/14
    // 672 hours * 60 min
    // stanford
    // 8/3/15 - 12/9/16 -- 494 days -- 12 hrs per day -- 5928 hours
    data.timeAvailable = timeAvailable

    // data.operationalMinutes = 5928 * 60
    // data.weeksAvailable = 70.5
    // data.hoursPerWeek = 84
    data.operationalMinutes = 672 * 60
    data.weeksAvailable = 10.87
    data.daysAvailable = 10.87 * 5
    data.hoursPerWeek = 60

    // site stats
    feed.stats = siteStats(data)

    feed.stats.operationalMinutes = data.operationalMinutes

    // 494 days -- 70.5 weeks ... 5928 / 70.5 
    // hours per week 12hrs * 7
    feed.stats.hoursPerWeek = data.hoursPerWeek

    // feed.buildings = buildingsArr
    feed.buildings = buildingStats(data)

    res.send(feed);
  })
}
