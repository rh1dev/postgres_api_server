module.exports = function(req, res, next) {

  var select = [
    'SELECT',
    'SUM(zero) AS zero,',
    'SUM(one) AS one,',
    'SUM(two) AS two,',
    'SUM(three) AS three,',
    'SUM(four) AS four,',
    'SUM(five) AS five,',
    'SUM(six) AS six,',
    'SUM(seven) AS seven,',
    'SUM(eight) AS eight,',
    'SUM(nine) AS nine,',
    'SUM(ten) AS ten,',
    'SUM(eleven) AS eleven,',
    'SUM(twelve) AS twelve,',
    'SUM(thirteen) AS thirteen,',
    'SUM(fourteen) AS fourteen,',
    'SUM(fifteen) AS fifteen,',
    'SUM(sixteen) AS sixteen,',
    'SUM(seventeen) AS seventeen,',
    'SUM(eighteen) AS eighteen,',
    'SUM(nineteen) AS nineteen,',
    'SUM(twenty) AS twenty,',
    'SUM(twentyone) AS twentyone,',
    'SUM(twentytwo) AS twentytwo,',
    'SUM(twentythree) AS twentythree',
    'FROM ' + req.tableName + '_class_hours AS c',
    'INNER JOIN ' + req.tableName + '_room AS r',
    'ON r.room_id = c.room_id',
    'INNER JOIN ' + req.tableName + '_building AS b',
    'ON r.bldg_id = b.bldg_id',
    req.whereClause,
  ].join(' ');

  var query = req.client.query(select);

  query.then(function(data) {
    res.send(data.rows)
  })
}
