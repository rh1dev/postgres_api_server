var _ = require('underscore')

module.exports = function(req, res, next) {
  // using max as aggregate funciton instead of last (not working in postgres)
  var select = [
    'SELECT r.scheduled_y AS scheduled,', 
    // 'b.bldg_id as building,',
    'b.bldg_alias as building,',
    'r.room_id AS room,', 
    // 'r.capacity AS capacity,', 
    'r.mCap AS capacity,', 
    'class_enroll AS enrollment,', 
    'start_time,',
    'end_time,',
    'EXTRACT(DOW FROM c.start_time) AS day,',
    'r.area_sf AS room_gsf,', 
    'class_id AS class',
    // 'FROM gse_class AS c',
    'FROM uhm_class AS c',
    // 'RIGHT OUTER JOIN gse_room AS r',
    'RIGHT OUTER JOIN uhm_room AS r',
    'ON c.room_id = r.room_id', 
    // 'JOIN gse_building AS b', 
    'JOIN uhm_building AS b', 
    'ON b.bldg_id = r.bldg_id',
    req.whereClause
  ].join(' ');

  console.log('select:', select)

  var query = req.client.query(select);

  query.then(function(data) {

    var feed = {}
    feed.rows = data.rows

    res.send(feed);
  })
}
