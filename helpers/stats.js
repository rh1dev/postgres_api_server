module.exports = function(data) {
  return {
    init: function() {
      return {
        roomsAvailable: 0,
        roomsScheduled: 0,
        minutesUsed: 0,
        totalEnrollment: 0,
        totalCapacity: 0,
        totalRoomsGSF: 0
      }
    },
    roomStats: function(data, obj) {
      
      obj.totalRoomsGSF += data.room_gsf || 0
      if (data.scheduled === 'y') {
        obj.minutesUsed += data.duration || 0
        obj.totalEnrollment += data.enrollment || 0
        obj.totalCapacity += data.capacity || 0
        obj.roomsScheduled += 1
      }
    },
    metrics: function(timeAvailable, obj) {
      obj.operationalMinutes = timeAvailable.minutes * obj.roomsScheduled // hours of operation times number of scheduled classes
      // obj.hoursPerWeekAvailable = timeAvailable.hours / timeAvailable.weeks
      // 5days * 12hrs per week
      obj.hoursPerWeekAvailable = 60 
      obj.hoursPerWeekUsed = ( (obj.minutesUsed / 60) / timeAvailable.weeks ) / obj.roomsScheduled
      obj.gsfPerPersonMax = obj.totalRoomsGSF / obj.totalCapacity
      obj.gsfPerPersonMin = obj.totalRoomsGSF / obj.roomsScheduled
      obj.gsfPerPersonUsed = obj.totalRoomsGSF / obj.totalEnrollment
      obj.density = obj.totalEnrollment / obj.totalCapacity
      obj.quantity = obj.roomsScheduled / obj.roomsAvailable
      obj.operations = obj.operationalMinutes > 0 ? obj.minutesUsed / obj.operationalMinutes : 0
      obj.seatHours = obj.density * obj.quantity * obj.operations
    }
  }
}
