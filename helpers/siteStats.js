var statsObj = require('./stats.js')

module.exports = function(data) {
  var stats = statsObj().init()
  stats.roomsAvailable = data.rows.length
  data.rows.forEach(function(item, i) {
    statsObj().roomStats(item, stats)
  })
  statsObj().metrics(data.timeAvailable, stats)
  return stats
}
