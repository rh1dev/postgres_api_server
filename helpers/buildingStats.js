var _ = require('underscore')
var statsObj = require('./stats.js')

module.exports = function(data) {
  var feed = []
  var buildings = {}
  data.rows.forEach(function(item, i) {
    var buildingId = item.building
    if (typeof buildings[buildingId] === 'undefined') {
      buildings[buildingId] = item
      buildings[buildingId].buildingStats = statsObj().init()
    } 
    var s = buildings[buildingId].buildingStats
    s.roomsAvailable += 1
    statsObj().roomStats(item, s)
  })

  _.forEach(buildings, function(e) {
    statsObj().metrics(data.timeAvailable, e.buildingStats)
    feed.push(e)
  })

  return feed
}
