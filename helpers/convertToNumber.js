module.exports = function(data) {
  for (var i = 0; i < data.rows.length; i++) {
    // postgres aggregates data as strings --  convert to numbers
    data.rows[i]['capacity'] = parseInt(data.rows[i]['capacity'])
    data.rows[i]['enrollment'] = parseInt(data.rows[i]['enrollment'])
    data.rows[i]['total_classes'] = parseInt(data.rows[i]['total_classes'])
    data.rows[i]['room_gsf'] = parseInt(data.rows[i]['room_gsf'])
  }
}
