module.exports = function(data, timeAvailable) {
  data.rows.forEach(function(item, i) {
    if (!item['duration'] > 0) {
      item['hoursPerWeekUsed'] = 0
    } else {
      item['hoursPerWeekUsed'] = (item['duration'] / 60) / timeAvailable.weeks
    }
    item.hoursPerWeekAvailable = timeAvailable.hours / timeAvailable.weeks
    item['gsfPerPersonMax'] = item.room_gsf / item.capacity
    item['gsfPerPersonMin'] = item.room_gsf
  })
}
