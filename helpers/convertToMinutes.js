module.exports = function(data) {
  for (var i = 0; i < data.rows.length; i++) {
    data.rows[i]['duration'] = calcDuration(i, data.rows[i]['duration'])
  }
}

function calcDuration(i, obj) {
  var num = 0;
  if (obj) {
    if (obj.hours) {
      num += (obj.hours * 60)
    }
    if (obj.minutes) {
      num += (obj.minutes)
    }
  }
  return num;
}